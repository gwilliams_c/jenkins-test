const { version: appVersion } = require('../../package.json');
import { MatSidenav } from '@angular/material/sidenav';
import { Component, ViewEncapsulation, ViewChild, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { LoopBackConfig } from 'shared/sdk';
import { UserApi } from 'shared/sdk/services';
import * as dsa from 'state-management/actions/datasets.actions';
import * as ua from 'state-management/actions/user.actions';
import { MatSnackBar } from '@angular/material';

// import { NotificationsService } from 'angular2-notifications';

import { environment } from '../environments/environment';
import * as selectors from 'state-management/selectors';
import { ParamsService } from 'params.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserApi],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnDestroy, OnInit {

  @ViewChild('sidenav') sidenav: MatSidenav;

  title = 'SciCat';
  appVersion = 0;
  us: UserApi;
  darkTheme$;
  username: string = null;
  message$ = null;
  msgClass$ = null;
  subscriptions = [];
  public options = {
    position: ['top', 'right'],
    lastOnBottom: true,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true,
    timeOut: 2000
  };

  constructor(private router: Router,
    public snackBar: MatSnackBar,
    private params: ParamsService,
    // private _notif_service: NotificationsService,
    private store: Store<any>) {
    this.appVersion = appVersion;
    this.darkTheme$ = this.store.select(selectors.users.getTheme);
  }

  /**
   * Handles initial check of username and updates
   * auth service (loopback does not by default)
   * @memberof AppComponent
   */
  ngOnInit() {
    LoopBackConfig.setBaseURL(environment.lbBaseURL);
    console.log(LoopBackConfig.getPath());
    if ('lbApiVersion' in environment) {
      const lbApiVersion = environment['lbApiVersion'];
      LoopBackConfig.setApiVersion(lbApiVersion);
    }

    localStorage.clear();
    if (window.location.pathname.indexOf('logout') !== -1) {
      this.logout();
      this.router.navigate(['/login']);
    }

    this.subscriptions.push(this.store.select(state => state.root.user.message)
      .subscribe(current => {
        if (current.content !== undefined) {
          this.snackBar.open(current.content, undefined, {
            duration: current.duration,
          });
          this.store.dispatch(new ua.ClearMessageAction());
        }
      }));
    this.subscriptions.push(this.store.select(state => state.root.user.currentUser)
      .subscribe(current => {
        if (current && current['username']) {
          this.username = current['username'].replace('ms-ad.', '');
          if (!('realm' in current)) {
            this.store.dispatch(new dsa.AddGroupsAction(current.id));
            this.store.dispatch(new ua.AccessUserEmailAction(this.username));
            // TODO handle dataset loading
          }
        } else if (current && current['loggedOut']) {
          if (window.location.pathname.indexOf('login') === -1) {
            window.location.replace('/login');
          }
        } else {
        }
      }));
    this.store.dispatch(new ua.RetrieveUserAction());
  }

  ngOnDestroy() {
    for (let i = 0; i < this.subscriptions.length; i++) {
      this.subscriptions[i].unsubscribe();
    }
  }

  logout() {
    this.store.dispatch(new ua.LogoutAction());
    this.router.navigate(['/login']);
  }

  login() {
    this.router.navigateByUrl('/login');
  }

  sidenavToggle() {
    console.log('closing?');
    this.sidenav.opened ? this.sidenav.close() : this.sidenav.open();
  }
}
