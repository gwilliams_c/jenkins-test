export const environment = {
  production: true,
  lbBaseURL: 'https://dacat.psi.ch',
  externalAuthEndpoint: '/auth/msad'
};
