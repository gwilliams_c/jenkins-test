export const environment = {
  production: true,
  lbBaseURL: 'https://dacat-staging.psi.ch',
  externalAuthEndpoint: '/auth/msad',
};
