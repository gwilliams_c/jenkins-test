export const environment = {
  production: true,
  lbBaseURL: 'https://dacat-qa.psi.ch',
  externalAuthEndpoint: '/auth/msad',
};
